tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
Integer ifIter = 0;
}

prog    : (e+=expr | e+=comp | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)                                -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                                -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                                -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                                -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($i1.text)}? -> setVariable(id={$ID.text},expr={$e2.st})
        | ID                       {globals.hasSymbol($ID.text)}? -> getVariable(id={$ID.text})
        | INT                                                     -> getInt(i={$INT.text})
        | ^(IF cond=comp e1=expr e1=expr) {++ifIter;}             -> ifCond(cond={$cond.st},true={$e1.st},false={$e2.st},ifIter={ifIter.toString()}) 
    ;
    
comp    : ^(EQ e1=expr e2=expr)  -> isEqual(p1={$e1.st},p2={$e2.st})
        | ^(NEQ e1=expr e2=expr) -> isEqual(p1={$e1.st},p2={$e2.st})
    ;
